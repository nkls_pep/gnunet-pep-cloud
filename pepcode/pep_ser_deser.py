import pEp
import email.utils
from lxml import etree

CT2TAG={
        'application/pgp-keys': 'keydata',
        'application/pEp.sync': 'sync',
        'application/pEp.distribution': 'distribution',
        'application/pgp-signature': 'signature',
}


TAG2CT=dict((v,k)for (k,v) in CT2TAG.items())

PEP_NAMESPACE="https://pEp.software/ns/pEp-1.0.xsd"
PEP_NS='{%s}' % PEP_NAMESPACE
NSMAP={'pEp': PEP_NAMESPACE}

UNENCRYPTED=0
INCOMING=0

def serialize(msg):
        root=etree.Element("msg",nsmap=NSMAP)
        etree.SubElement(root,"to").text=str(msg.to[0])
        etree.SubElement(root,"from").text=str(msg.from_)
        if msg.enc_format==UNENCRYPTED:
                etree.SubElement(root,"body").text=msg.longmsg
                attachments=etree.SubElement(root,"attachments")
                attachments=etree.SubElement(attachments,PEP_NS+"attachments",version=msg.opt_fields['X-pEp-Version'])
                for attach in msg.attachments:
                        a=etree.SubElement(attachments,PEP_NS+CT2TAG[attach.mime_type])
                        a.text=attach.decode("ascii")
        else:
                etree.SubElement(root,"body")
                attachments=etree.SubElement(root,"attachments")
                assert len(msg.attachments)==2
                attach=msg.attachments[1]
                n=etree.SubElement(root,PEP_NS+"messaage")
                n.text=attach.decode("ascii")
        return etree.tostring(root)

def serialize(msg):
        root=etree.Element("msg",nsmap=NSMAP)
        etree.SubElement(root,"to").text=str(msg.to[0])
        etree.SubElement(root,"from").text=str(msg.from_)
        if msg.enc_format==UNENCRYPTED:
                etree.SubElement(root,"body").text=msg.longmsg
                attachments=etree.SubElement(root,"attachments")
                attachments=etree.SubElement(attachments,PEP_NS+"attachments",version=msg.opt_fields['X-pEp-Version'])
                for attach in msg.attachments:
                        a=etree.SubElement(attachments,PEP_NS+CT2TAG[attach.mime_type])
                        a.text=attach.decode("ascii")
        else:
                etree.SubElement(root,"body")
                attachments=etree.SubElement(root,"attachments")
                assert len(msg.attachments)==2
                attach=msg.attachments[1]
                n=etree.SubElement(root,PEP_NS+"messaage")
                n.text=attach.decode("ascii")
        return etree.tostring(root)

