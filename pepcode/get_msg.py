import pep_ser_deser
import pEp

id_alice=pEp.Identity("alice@pep.foundation", "Alice", "1")
#print("Generated Identity",id_alice)

id_bob=pEp.Identity("bob@pep.foundation", "Bob", "2")
#print("Generated Identity",id_bob)

m=pEp.outgoing_message(id_alice)
m.to=[id_bob]
m.shortmsg="this is the shortmsg"
m.longmsg="this is the longmsg"
m_enc=m.encrypt()

m_ser=pep_ser_deser.serialize(m_enc)

print(m_ser)

