import pEp
import email.utils
from lxml import etree

CT2TAG={
	'application/pgp-keys': 'keydata',
	'application/pEp.sync': 'sync',
	'application/pEp.distribution': 'distribution',
	'application/pgp-signature': 'signature',
}


TAG2CT=dict((v,k)for (k,v) in CT2TAG.items())

PEP_NAMESPACE="https://pEp.software/ns/pEp-1.0.xsd"
PEP_NS='{%s}' % PEP_NAMESPACE
NSMAP={'pEp': PEP_NAMESPACE}

UNENCRYPTED=0
INCOMING=0


def serialize(msg):
	root=etree.Element("msg",nsmap=NSMAP)
	etree.SubElement(root,"to").text=str(msg.to[0])
	etree.SubElement(root,"from").text=str(msg.from_)
	if msg.enc_format==UNENCRYPTED:
		etree.SubElement(root,"body").text=msg.longmsg
		attachments=etree.SubElement(root,"attachments")
		attachments=etree.SubElement(attachments,PEP_NS+"attachments",version=msg.opt_fields['X-pEp-Version'])
		for attach in msg.attachments:
			a=etree.SubElement(attachments,PEP_NS+CT2TAG[attach.mime_type])
			a.text=attach.decode("ascii")
	else:
		etree.SubElement(root,"body")
		attachments=etree.SubElement(root,"attachments")
		assert len(msg.attachments)==2
		attach=msg.attachments[1]
		n=etree.SubElement(root,PEP_NS+"messaage")
		n.text=attach.decode("ascii")
	return etree.tostring(root)

def deserialize(msg_ser):
	
	def addr2identity(text):
		name,addr=email.utils.parseaddr(text)
		ident=pEp.Identity(addr,name)
		ident.update()
		return ident

	root=etree.fromstring(msg_ser)
	from_=addr2identity(root.xpath("./from/text()")[0])
	msg1=pEp.Message(INCOMING,from_)
	msg1.to.append(addr2identity(root.xpath("./to/text()")[0]))
	msg_enc=root.find("{%s}message" % PEP_NAMESPACE)
	if msg_enc is not None:
		msg1.attachments=[
			pEp.Blob(b"Version: 1","application/pgp-encrypted"),
			pEp.Blob(msg_enc.text.encode(), "application/xxpgp-encrypted")]
	else:
		msg1.longmsg=root.findtext("body")
		pEp_attachments=None
		attachments=root.find("attachments")
		if attachments is not None:
			pEp_attachments=attachments.find("{%s}attachments" % PEP_NAMESPACE)
		if pEp_attachments is not None:
			msg1.opt_fields['X-pEp-Version']=pEp_attachments.attrib["version"]
			pEp_attachs=[]
			for tagname in ("keydata","signature","sync","distribution"):
				for attach in pEp_attachments.iterfind("{%s}%s" % (PEP_NAMESPACE,tagname)):
					pEp_attachs.append(pEp.Blob(attach.text.encode(),TAG2CT[tagname]))
			msg1.attachments=pEp_attachs
	return msg1















id_alice=pEp.Identity("alice@pep.foundation", "Alice", "1")
print("Generated Identity",id_alice)

id_bob=pEp.Identity("bob@pep.foundation", "Bob", "2")
print("Generated Identity",id_bob)

m=pEp.outgoing_message(id_alice)
m.to=[id_bob]
m.shortmsg="this is the shortmsg"
m.longmsg="this is the longmsg"
m_enc=m.encrypt()

m_ser=serialize(m_enc)
#print("serialized message: ",m_ser)

m_deser=deserialize(m_ser)
#print("deserialized message: ",m_deser)

m_dec, keys,rating,flags=m_deser.decrypt()
print("decrypted message: ",m_dec)
print("keys: ",keys)
print("rating: ",rating)
print("flags: ",flags)



